/**
 * @param {number} number
 * @param {"float" | "integer"} type
 * @param {Object} [numberRange]
 * @param {number} [numberRange.min]
 * @param {number} [numberRange.max]
 * @returns {boolean}
 */
const isValidNumber = (number, type, numberRange) => {
  let isValid = false;
  const castedNumber = Number(number);

  if (!number || isNaN(castedNumber)) {
    return false;
  }

  if (type === "float") {
    isValid = castedNumber !== Math.floor(castedNumber);
  } else {
    isValid = Number.isInteger(castedNumber);
  }

  if (numberRange && isValid) {
    isValid = castedNumber >= numberRange.min && castedNumber <= numberRange.max;
  }

  return isValid;
};

/**
 * @param {string} text
 * @param {number} maxLength
 * @param {regex} regex
 * @returns {boolean}
 */
const isValidString = (text, maxLength, regex) => {
  if (!text || !isNaN(Number(text))) {
    return false;
  }

  if (regex) {
    const matchs = text.match(regex);

    if (!matchs) return false;
  }

  return text.length <= maxLength;
};

module.exports = {
  isValidNumber,
  isValidString,
};
