# Constlidator

Assert your constants meet your specified requirements

## Installation

Go to [constlidator npm](https://www.npmjs.com/package/constlidator)

Install the package using npm: 
```console
~$ npm i constlidator
```

## Usage

### isValidNumber

The `isValidNumber` function validates a number based on the provided parameters.

#### Parameters

- `number`: The number to validate.
- `type`: The type of number to expect ("float" or "integer").
- `numberRange`: (Optional) An object defining the range for the number.
  - `min`: The minimum value of the range.
  - `max`: The maximum value of the range.

#### Usage Example

```javascript
const { isValidNumber } = require('constlidator');

const number = 10;
const type = 'integer';
const range = { min: 0, max: 100 };

const isValid = isValidNumber(number, type, range);
console.log(isValid); // Output: true or false
```

### isValidString

The `isValidString` function validates a string based on the provided parameters.

#### Parameters

- `text`: The string to validate.
- `maxLength`: The maximum length allowed for the string.
- `regex`: (Optional) A regular expression to match against the string.

#### Usage Example

```javascript
const { isValidString } = require('constlidator');

const text = 'example';
const maxLength = 10;
const regex = /^[a-zA-Z]+$/; // Example regex to match only letters

const isValid = isValidString(text, maxLength, regex);
console.log(isValid); // Output: true or false
```
